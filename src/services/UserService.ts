import bcrypt from "bcrypt";
import UserModel from "../models/UserModel";
import { config } from "../configs";
import { generateID } from "../utils";
import { userType } from "../constants/user";
import { IUpdateUser, IUser } from "../types/User";
import jwt from "jsonwebtoken";

export const getUserByEmailAddress = async (emailAddress: string) => {
  return await UserModel.findOne({ emailAddress });
};
export const getUserByUserID = async (
  userID: string,
  selected: string
): Promise<IUser> => {
  console.log({ userID });
  return await UserModel.findOne({ userID }).select(selected);
};
export const isRegisteredOnSmileID = async (userID: string) => {
  const user = await getUserByUserID(userID, "smileID");
  console.log(user?.userMeta?.smileID)
  return user?.userMeta?.smileID ? true : false;
};
export const registerUserWithEmail = async (emailAddress: string) => {
  const user = {
    emailAddress,
    userID: generateID("user"),
    userType: userType.user,
  };
  return await UserModel.create(user);
};

export const updateUserByUserID = async (userID: string, user: IUpdateUser) => {
  return await UserModel.findOneAndUpdate({ userID }, user);
};

//password related
export const hashPassword = async (password: string) => {
  console.log(config.app.SALT_ROUNDS);
  const hash = await bcrypt.hashSync(password, config.app.SALT_ROUNDS);
  return hash;
};
export const verifyHashedPassword = async (
  password: string,
  hashedPassword: string
) => {
  return await bcrypt.compareSync(password, hashedPassword);
};

export const generateUserJWTtoken = async (user: IUser) => {
  const jwtUser = {
    emailAddress: user?.emailAddress,
    userID: user?.userID,
  };
  const token = jwt.sign(
    {
      data: jwtUser,
    },
    config.jwt.JWT_SECRET_KEY,
    { expiresIn: config.jwt.TOKEN_EXPIRY }
  );
  return {
    token,
    user: jwtUser,
  };
};
