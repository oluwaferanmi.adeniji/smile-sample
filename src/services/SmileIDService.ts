import { config } from "../configs";
import axios from "axios";
import { generateID } from "../utils";
const configs = config.smileID;
import fs from "fs";
import archiver from "archiver";
import path from "path";
const smileIdentityCore = require("smile-identity-core");

type SmileIDService = {
  prepJob: (user: any, smartSelfie?: boolean) => Promise<any>;
  submitJob: (
    job_id: string,
    images: any,
    smileUploadUrl: string
  ) => Promise<any>;
  submitJobViaPackage: (
    job_id: string,
    images: any,
    userID: string,
    jobType: number
  ) => Promise<any>;
};
const default_callback =
  "https://webhook.site/71d2692b-274f-4c08-a8da-994d782f4bf2";
export const generateSignature = () => {
  const Signature = smileIdentityCore.Signature;
  const connection = new Signature(configs.PARTNER_ID, configs.API_KEY);

  // Generate the Signature
  //console.log(new Date().toISOString())
  const generated_signature = connection.generate_signature(
    new Date().toISOString()
  ); // where timestamp is optional
  return generated_signature;
};
const prepJob = async (user: any, smartSelfie: boolean = false) => {
  //console.log(configs);
  const signature = generateSignature();
  const job_id = generateID("smilejob");
  let data = JSON.stringify({
    callback_url: default_callback,
    model_parameters: {},
    partner_params: {
      job_id,
      job_type: smartSelfie ? 4 : 1 ,
      user_id: user.userID,
    },
    signature: signature.signature,
    smile_client_id: configs.PARTNER_ID,
    source_sdk_version: "1.0.0",
    source_sdk: "rest_api",
    country: "NG",
    timestamp: signature.timestamp,
  });

  let config = {
    method: "post",
    url: "https://testapi.smileidentity.com/v1/upload",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  try {
    const response = await axios(config);
    console.log(response.data);
    return {
      ...response.data,
      message: "Smile ID job created successfully",
      job_id,
    };
  } catch (error) {
    console.error(error);
    return null;
  }
};
const enrollUser = async (user: any, smartSelfie: boolean = false) => {
  //console.log(configs);
  const signature = generateSignature();
  const job_id = generateID("smilejob");
  let data = JSON.stringify({
    callback_url: default_callback,
    model_parameters: {},
    partner_params: {
      job_id,
      job_type: smartSelfie ? 4 : 1 ,
      user_id: user.userID,
    },
    signature: signature.signature,
    smile_client_id: configs.PARTNER_ID,
    source_sdk_version: "1.0.0",
    source_sdk: "rest_api",
    country: "NG",
    timestamp: signature.timestamp,
  });

  let config = {
    method: "post",
    url: "https://testapi.smileidentity.com/v1/upload",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };
  try {
    const response = await axios(config);
    console.log(response.data);
    return {
      ...response.data,
      message: "Smile ID job created successfully",
      job_id,
    };
  } catch (error) {
    console.error(error);
    return null;
  }
};
const submitJob = async (
  job_id: string,
  images: any,
  smileUploadUrl: string
) => {
  // Step 1: Create JSON file
  const jsonFileContent = {
    package_information: {
      apiVersion: {
        buildNumber: 0,
        majorVersion: 2,
        minorVersion: 0,
      },
    },
    id_info: {
      country: "NG",
      entered: 1,
      last_name: "Joe",
      first_name: "Leo",
      middle_name: "Doe",
      job_type: 1,
    },
    images,
  };
  const jsonFilePath = path.join(__dirname, "info.json");
  fs.writeFileSync(
    jsonFilePath,
    JSON.stringify(jsonFileContent, null, 2),
    "utf-8"
  );

  // Step 2: Zip the JSON file
  const zipFilePath = path.join(__dirname, "info.zip");
  const output = fs.createWriteStream(zipFilePath);
  const archive = archiver("zip", { zlib: { level: 9 } });

  return new Promise((resolve, reject) => {
    output.on("close", async () => {
      console.log(
        `info.zip has been created. Total bytes: ${archive.pointer()}`
      );

      // Step 3: Upload the zip file to S3 pre-signed URL
      try {
        const zipFileContent = fs.readFileSync(zipFilePath);
        const submit = await axios.put(smileUploadUrl, zipFileContent, {
          headers: {
            "Content-Type": "application/zip",
          },
        });
        console.log(submit.data);
        resolve({
          message: "Images uploaded successfully",
          smileUploadUrl,
          success: true,
        });
      } catch (err) {
        console.error("Error uploading file:", err);
        reject({ message: "Error uploading file", success: false });
      } finally {
        // Cleanup: delete the created files
        fs.unlinkSync(jsonFilePath);
        fs.unlinkSync(zipFilePath);
      }
    });

    archive.on("error", (err) => {
      console.error("Archiving error:", err);
      reject({ message: "Error zipping file", success: false });
    });

    archive.pipe(output);
    archive.file(jsonFilePath, { name: "info.json" });
    archive.finalize();
  });
};
const submitJobViaPackage = async (
  job_id: string,
  images: any,
  userID: string,
  jobType: number
) => {
  const WebApi = smileIdentityCore.WebApi;
  const type = smileIdentityCore.JOB_TYPE;
  console.log(type)
  let sid_server = 0; // Use '0' for the sandbox server, use '1' for production server

  const connection = new WebApi(
    configs.PARTNER_ID,
    default_callback,
    configs.API_KEY,
    sid_server
  );
  const smartSelfie = jobType === 2 || jobType === 4;
  
  // Create required tracking parameters
  let partner_params = {
    job_id: job_id,
    user_id: userID,
    job_type: jobType,
  };
  let image_details = images;
  let id_info = {
    last_name: "Joe",
    first_name: "Leo",
    middle_name: "Doe",
    country: "NG",
    id_type: "NIN_V2",
    id_number: "00000000000",
    dob: "21-04-12", // yyyy-mm-dd
    entered: "true", // must be a string
  };
  console.log(smartSelfie)
  // Set the options for the job
  let options = {
    return_job_status: true, // Set to true if you want to get the job result in sync (in addition to the result been sent to your callback). If set to false, result is sent to callback url only.
    return_history: false, // Set to true to return results of all jobs you have ran for the user in addition to current job result. You must set return_job_status to true to use this flag.
    return_image_links: true, // Set to true to receive selfie and liveness images you uploaded. You must set return_job_status to true to use this flag.
    signature: true,
  };
  try {

    const response = await connection.submit_job(
      partner_params,
      image_details,
      smartSelfie ? {} : id_info,
      options
    );
  console.log('returned')
  return response;

  }
  catch (error) {
    console.error(error);
    return {
      success: false
    };
  }
};

const submitSmartSelfieJob = async (
  job_id: string,
  images: any,
  userID: string
) => {

}
export const SmileIDService: SmileIDService = {
  prepJob,
  submitJob,
  submitJobViaPackage,
};
