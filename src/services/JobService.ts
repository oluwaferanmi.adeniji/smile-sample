import SmileJobs from "../models/SmileJobs";

export const getJobByJobID = async (jobID: string) => {
  return await SmileJobs.findOne({ jobID });
};
export const createJob = async (userID: string, jobID: string) => {
  const job = {
    userID,
    jobID,
    status: "pending",
    statusHistory: [
      {
        status: "pending",
        timestamp: new Date(),
      },
    ],
  };
  return await SmileJobs.create(job);
};
export const updateJobStatusByJobID = async (jobID: string, status: string, json?: any) => {
  return await SmileJobs.findOneAndUpdate(
    { jobID },
    { status, $push: { statusHistory: { status, timestamp: new Date() }, json } }
  );
};
