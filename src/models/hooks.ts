import mongoose from "mongoose";
const Schema = mongoose.Schema;

const hooksSchema = new Schema(
  {
    hookPayload: { type: Object, required: true },
    hookID: { type: String, required: true, unique: true },
  },
  {
    timestamps: true,
  }
);
exports.hooks = hooksSchema;
export default mongoose.model("hooks", hooksSchema);
