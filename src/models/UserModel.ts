import mongoose from "mongoose";
import { userType } from "../constants/user";
const Schema = mongoose.Schema;

export const userSchema = new Schema(
  {
    userID: { type: String, required: true, unique: true, immutable: true },
    firstName: { type: String, default: "" },
    lastName: { type: String, default: "" },
    emailAddress: { type: String, required: true, unique: true },
    password: { type: String, default: "" },
    role: { type: String, default: userType.user },
    userType: { type: String, default: userType.user },
    userActiveStatus: { type: Boolean, default: true },
    userStatus: { type: String, default: ""},
    verificationFlags: {
      type: Object,
      default: {
        email: false,
        passwordSetup: false,
      }  
    },
    userMeta: {
        type: Object,
        default: {
            profileImageKey: "",
            profileImageBase64: "",
        }
    },
    settings: {
        type: Object,
        default: {
            emailNotification: false,
            TwoFASetup: false,
        },
        }
    },
  {
    timestamps: true,
  }
);
exports.User = userSchema;
export default mongoose.model("users", userSchema);