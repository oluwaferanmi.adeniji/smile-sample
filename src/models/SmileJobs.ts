import mongoose from "mongoose";
const Schema = mongoose.Schema;

const jobsSchema = new Schema(
  {
    userID: { type: String, required: true },
    status: { type: String, required: true },
    json: { type: Object },
    jobID: { type: String, required: true, unique: true },
    statusHistory: { type: Array, default: [] },
  },
  {
    timestamps: true,
  }
);
exports.jobs = jobsSchema;
export default mongoose.model("smileJobs", jobsSchema);
