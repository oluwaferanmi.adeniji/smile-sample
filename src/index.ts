import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import { config } from "./configs";
import mongoose from "mongoose";
import userRouter from "./routes/userRouter";

const cors = require("cors");

const app = express();
const port = config.app.PORT;

//configs
app.use(cors({ origin: "*" }));
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));

// handle malformed json body
app.use((err: any, req: Request, res: Response, next: any) => {
  if (err) {
    console.log(err);
    return res.status(400).send("Invalid JSON payload passed.");
    //exit if payload is not a valid json and cannot be processed
  }
  next();
});

//routes
app.use("/v1/user", userRouter);

app.all("/health", (req: Request, res: Response) => {
  res.status(200).send({
    message: "Service is up and running",
  });
});

// handle 404s
app.use((req: Request, res: Response) => {
  res.status(404).send({
    message: "URL Not found",
  });
});

mongoose
  .connect(config.db.MONGO_URI, {
    dbName: config.db.DB_NAME,
  })
  .then(() => console.log("connected to mongodb"))
  .catch((err) => console.log("error occured connecting to mongodb", err));

app.listen(port, () => {
  console.log(`Message service listening at ${port}`);
});

process.on("beforeExit", (code) => {
  // Can make asynchronous calls
  setTimeout(() => {
    console.log(`Process will exit with code: ${code}`);
    process.exit(code);
  }, 100);
});
process.on('uncaughtException', err => {
  console.log(`Uncaught Exception: ${err.message}`)
  process.exit(1)
})

process.on("exit", (code) => {
  // Only synchronous calls
  console.log(`Process exited with code: ${code}`);
});
