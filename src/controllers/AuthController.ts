import { AuthResponses } from "../constants/authResponses";
import { generateSignature, SmileIDService } from "../services/SmileIDService";
import {
  generateUserJWTtoken,
  getUserByEmailAddress,
  hashPassword,
  registerUserWithEmail,
  updateUserByUserID,
  verifyHashedPassword,
} from "../services/UserService";
import { IUpdateUser, IUser } from "../types/User";
import { WrapValidationHandler } from "../utils";
import { Request, Response } from "express";
import { enrollUser } from "./SmileIDController";

export const UserLoginHandler = WrapValidationHandler(
  async (req: Request, res: Response) => {
    const { email, password } = req.body;
    const doesEmailExist = await getUserByEmailAddress(email);
    if (!doesEmailExist)
      return res.status(400).send({ message: AuthResponses.INCORRECT_LOGIN });
    const user = doesEmailExist as IUser;

    const hashedPassword = user.password as string;
    const comparePassword = await verifyHashedPassword(
      password,
      hashedPassword
    );
    if (!comparePassword)
      return res.status(400).send({ message: AuthResponses.INCORRECT_LOGIN });
    const login = await generateUserJWTtoken(user);
    res.send({
      ...login,
      message: AuthResponses.LOGIN_SUCCESS,
    });
  }
);
export const SignupController = WrapValidationHandler(
  async (req: Request, res: Response) => {
    // create a new user
    // validate if user email already exist in the database
    const { email, password } = req.body;
    const userExists = await getUserByEmailAddress(email);
    if (userExists) {
      return res.status(400).send({ message: "User already exists" });
    }
    // create a new user
    const user = await registerUserWithEmail(email);
    console.log(user);
    const passwordUpdate: IUpdateUser = {
      password: await hashPassword(password),
      verificationFlags: {
        email: true,
        passwordSetup: true,
      },
    };

    // update the user password
    await updateUserByUserID(user.userID, {
      ...passwordUpdate,
    });
    // generate a token
    const token = await generateUserJWTtoken(user as IUser);
    return res
      .status(200)
      .send({ message: "User signed up successfully", ...token });
  }
);
export const ImageUploadController = WrapValidationHandler(
  async (req: any, res: Response) => {
    const file = req.file;
    if (!file)
      return res
        .status(400)
        .send({ success: false, message: "No file uploaded" });
    const user = req.user as IUser;
    const encoded = req.file.buffer.toString("base64");
    console.log(encoded);
    // create user on smileID
    const smileData = await enrollUser(user, encoded);
    console.log(smileData);
    const userUpdate: IUpdateUser = {
      userMeta: {
        profileImageBase64: encoded,
        smileData,
      },
    };
    await updateUserByUserID(user.userID, userUpdate);
    return res.status(smileData.success ? 200 : 400).send({
      success: true,
      message: smileData.success ? "Image uploaded successfully" : smileData.message,
      smileData,
    });
  }
);
