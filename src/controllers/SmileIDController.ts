import {
  getUserByUserID,
  isRegisteredOnSmileID,
} from "../services/UserService";
import { generateID, WrapValidationHandler } from "../utils";
import { Request, Response } from "express";
import { SmileIDService } from "../services/SmileIDService";
import { createJob, updateJobStatusByJobID } from "../services/JobService";
import { SmileJobStatus } from "../constants";

const useSmileIDPackage = true;
const useBiometricKyc = false;

export const UploadSmileIDImagesController = WrapValidationHandler(
  async (req: any, res: Response) => {
    const images = req.body.images;
    const userID = req.userID;
    const user = await getUserByUserID(userID, "");
    // prep job

    if (useBiometricKyc) {
      return biometricVerification({ images, userID, user, res });
    }
    // use the smart selfie package
    smartSelfieVerification({ images, userID, user, res });
  }
);
export const getSmartSelfieJobType = async (user: any) => {
  // if user already registed, we use 4, otherwise 2
  const hasRegistered = await isRegisteredOnSmileID(user.userID);
  console.log(hasRegistered);
  return hasRegistered ? 4 : 2;
};
export const enrollUser = async (user: any, selfieBase64: string) => {
  const jobID = generateID("job");
  createJob(user?.userID, jobID);
  const smileImages = [
    {
      image_type_id: 2,
      image: selfieBase64,
      file_name: "",
    },
  ];
  const smileJob = await SmileIDService.submitJobViaPackage(
    jobID,
    smileImages,
    user?.userID,
    4
  );
  updateJobStatusByJobID(jobID, SmileJobStatus.submitted);
  updateJobStatusByJobID(
    jobID,
    smileJob.job_success
      ? SmileJobStatus.submission_completed
      : SmileJobStatus.submission_failed,
    smileJob
  );
  console.log(smileJob);
  if (!smileJob.job_success)
    return {
      success: false,
      message: smileJob?.result?.ResultText || "Failed",
    };

  return {
    success: true,
    message: "user enrolled successfully",
  };
};
const smartSelfieVerification = async ({
  images,
  userID,
  user,
  res,
}: {
  images: [];
  userID: string;
  user: any;
  res: Response;
}) => {
  const jobID = generateID("job");
  createJob(userID, jobID);

  const jobType = await getSmartSelfieJobType(user);
  console.log(jobType);
  const smileJob = await SmileIDService.submitJobViaPackage(
    jobID,
    images,
    userID,
    2
  );
  updateJobStatusByJobID(jobID, SmileJobStatus.submitted);
  updateJobStatusByJobID(
    jobID,
    smileJob.success
      ? SmileJobStatus.submission_completed
      : SmileJobStatus.submission_failed,
    smileJob
  );
  if (!smileJob)
    return res
      .status(400)
      .send({ success: false, message: "Smile ID job submission failed" });
  return res.status(200).send({
    success: true,
    message: "Smile ID job submitted successfully",
    result: smileJob.result,
    images: [
      ...images,
      {
        image_type_id: 2,
        image: user?.userMeta?.profileImageBase64,
        file_name: "",
      },
    ],
  });
};
const biometricVerification = async ({
  images,
  userID,
  user,
  res,
}: {
  images: [];
  userID: string;
  user: any;
  res: Response;
}) => {
  if (useSmileIDPackage) {
    const jobID = generateID("job");
    createJob(userID, jobID);
    console.log(images.length);
    const livenessImages = images.filter(
      (image: any) => image.image_type_id === 6
    );
    console.log(livenessImages.length);
    const smileImages = [
      {
        image_type_id: 2,
        image: user?.userMeta?.profileImageBase64,
        file_name: "",
      },
      ...livenessImages,
    ];
    console.log(smileImages.length);
    console.log({
      image_type_id: 2,
      image: user?.userMeta?.profileImageBase64.split("/")[1],
      file_name: "",
    });
    const smileJob = await SmileIDService.submitJobViaPackage(
      jobID,
      images,
      userID,
      1
    );
    console.log(smileJob);
    updateJobStatusByJobID(jobID, SmileJobStatus.submitted);
    updateJobStatusByJobID(
      jobID,
      smileJob.success
        ? SmileJobStatus.submission_completed
        : SmileJobStatus.submission_failed,
      smileJob
    );
    if (!smileJob)
      return res
        .status(400)
        .send({ success: false, message: "Smile ID job submission failed" });
    return res.status(200).send({
      success: true,
      message: "Smile ID job submitted successfully",
      result: smileJob.result,
      images: smileImages,
    });
  }
  const smileJob = await SmileIDService.prepJob(user);
  console.log(smileJob);
  if (!smileJob)
    return res
      .status(400)
      .send({ success: false, message: "Smile ID job prep failed" });
  createJob(userID, smileJob.job_id);
  if (!smileJob)
    return res
      .status(400)
      .send({ success: false, message: "Smile ID job prep failed" });
  const smileUploadUrl = smileJob?.upload_url;
  if (!smileUploadUrl) {
    return res.status(400).send({ message: "SmileID not initiated" });
  }
  const livenessImages = images.filter(
    (image: any) => image.image_type_id === 6
  );
  const smileImages = [
    {
      image_type_id: 2,
      image: user?.userMeta?.profileImageBase64,
      file_name: "",
    },
    ...livenessImages,
  ];
  // submit job
  const submitJob = await SmileIDService.submitJob(
    smileJob.job_id,
    smileImages,
    smileUploadUrl
  );
  console.log(submitJob);
  updateJobStatusByJobID(smileJob.job_id, SmileJobStatus.submitted);
  if (!submitJob.success) {
    updateJobStatusByJobID(smileJob.job_id, SmileJobStatus.submission_failed);
    return res
      .status(400)
      .send({ success: false, message: "Smile ID job submission failed" });
  }
  updateJobStatusByJobID(smileJob.job_id, SmileJobStatus.submission_completed);

  return res.status(200).send({
    success: true,
    message: "Smile ID job submitted successfully",
  });
};
