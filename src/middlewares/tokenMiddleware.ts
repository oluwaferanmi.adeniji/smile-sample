import jwt from "jsonwebtoken";
import { config } from "../configs";
import { IUser } from "../types/User";

export const UserTokenMiddleware = async (req: any, res: any, next: any) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).send({
      message: "Unauthorized",
    });
  }
  // cut the token from the string
  const tokenString = token.split(" ")[1];
  // verify token
  const decodedUser = await verifyToken(tokenString);
  // if token is valid, call next
  if (!decodedUser) {
    return res.status(401).send({ message: "Unauthorized" });
  }
  const user = decodedUser.data as IUser;
  req.token = tokenString;
  req.user = user;
  req.userID = user.userID;
  return next();
};
const verifyToken = (token: string) => {
  const jwtSecretKey = config.jwt.JWT_SECRET_KEY;
  try {
    const decoded = jwt.verify(token, jwtSecretKey);
    // console.log(decoded);
    return decoded as {
      data: object,
    };
  } catch (error) {
    // Access Denied
    console.log(error);
    return false;
  }
};
