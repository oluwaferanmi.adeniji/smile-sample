import { config as dotEnvConfig } from "dotenv";

dotEnvConfig();
export const config = {
  app: {
    PORT: process.env.PORT || 5007,
    SALT_ROUNDS: 10,
  },
  jwt: {
    TOKEN_EXPIRY: parseInt(process.env.TOKEN_EXPIRY || "3600"),
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || "",
  },
  db: {
    MONGO_URI: process.env.MONGO_URI || "",
    DB_NAME: process.env.DB_NAME || "",
  },
  aws: {
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID || "",
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || "",
    AWS_REGION: process.env.AWS_REGION || "",
  },
  smileID: {
    SIGNATURE: process.env.SMILE_ID_SIGNATURE || "",
    PARTNER_ID: process.env.SMILE_ID_PARTNER_ID || "",
    API_KEY: process.env.SMILE_ID_API_KEY || "",
  },
  serviceURIs: {},
};
