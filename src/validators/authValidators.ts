import { body } from "express-validator";

export const loginValidator = [
  body("email", "Email cannot be Empty").not().isEmpty(),
  body("email", "Invalid email").isEmail(),
  body("password", "The minimum password length is 4 characters").isLength({
    min: 4,
  }),
];
export const emailLoginValidator = [
  body("email", "Email cannot be Empty").not().isEmpty(),
  body("email", "Invalid email").isEmail(),
];
export const signupValidator = [
  body("email", "Email cannot be Empty").not().isEmpty(),
  body("email", "Invalid email").isEmail(),
  body("password", "Password cannot be Empty").not().isEmpty(),
];
export const smileIDImageUploadValidator = [
  //validate images present, an array of images and should be equal to 9 items
  body("images", "Images cannot be Empty").not().isEmpty(),
  body("images", "Images must be an array").isArray(),
]