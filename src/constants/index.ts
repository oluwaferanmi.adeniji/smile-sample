export const allCharacters =
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

export const SmileJobStatus = {
  pending: "pending",
  completed: "job_completed_success",
  submission_failed: "submission_failed",
  submission_completed: "submission_completed",
  submitted: "submitted",
  failed: "job_completed_failed",
};
