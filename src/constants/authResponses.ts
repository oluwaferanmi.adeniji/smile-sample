export const AuthResponses = {
    EMAIL_EXIST:
      "An account with this email address is already registered, please login or use another email address",
    USER_REGISTRATION_SUCCESSFUL: "User registration successful",
    INCORRECT_LOGIN: "Incorrect email address or password",
    LOGIN_SUCCESS: "Logged in successful",
  };