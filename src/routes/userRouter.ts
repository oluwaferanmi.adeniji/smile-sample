import { Router } from "express";
import {
  loginValidator,
  signupValidator,
  smileIDImageUploadValidator,
} from "../validators/authValidators";
import {
  ImageUploadController,
  SignupController,
  UserLoginHandler,
} from "../controllers/AuthController";
import multer from "multer";
import { UserTokenMiddleware } from "../middlewares/tokenMiddleware";
import { UploadSmileIDImagesController } from "../controllers/SmileIDController";

const userRouter = Router();
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

userRouter.post("/signup", signupValidator, SignupController);
userRouter.post("/login", loginValidator, UserLoginHandler);
userRouter.post(
  "/upload",
  upload.single("file"),
  UserTokenMiddleware,
  ImageUploadController
);
userRouter.post(
  "/smileid/upload-smiles",
  smileIDImageUploadValidator,
  UserTokenMiddleware,
  UploadSmileIDImagesController
);

export default userRouter;
