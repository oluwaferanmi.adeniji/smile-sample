import { userType } from "../constants/user";

export type IUser = {
  userID: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
  password?: string;
  userActiveStatus: Boolean;
  userStatus: string;
  verificationFlags?: IVerificationFlags;
  userMeta?: any;
  settings?: Object;
};
export type IUpdateUser = {
  userID?: string;
  firstName?: string;
  lastName?: string;
  emailAddress?: string;
  password?: string;
  userActiveStatus?: Boolean;
  userStatus?: string;
  verificationFlags?: IVerificationFlags;
  userMeta?: any;
  settings?: Object;
};

export type IVerificationFlags = {
  email: boolean;
  passwordSetup: Boolean;
};
