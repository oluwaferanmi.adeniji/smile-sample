export type ICreateCoach = {
    coachCompanyName: string;
    coachCompanyDomain: string;
    userID: string;
    coachID: string;
}
export type ICoach = {
    coachID: string;
    userID: string;
    coachCompanyName: string;
    coachCompanyDomain: string;
    userActiveStatus: boolean;
    coachStatus: string;
    coachPlan: string;
    coachOnTrialPlan: boolean;
    coachPlanMeta: {
        plan: string;
        planStartDate: Date;
        nextBillingDate: Date;
    };
    coachMeta: {
        profileImageKey: string;
    };
    settings: Object;
}